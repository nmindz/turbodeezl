# TurboDeezl

A lazy wrapper-on-wrapper for the Deezer API, in order to make tasks like creating huge playlists (i.e.: add ALL of a given artists tracks to a given playlist) a breeze.

Designed out of an (ab)use of tools designed to read from playlists and perform mass actions on them, just because I am too lazy to do it by hand. Plus, what's the fun on doing it like everyone else?
